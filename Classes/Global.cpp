#include "Global.h"

Global* Global::_instance = nullptr;

Global::Global()
{
}

Global* Global::getInstance()
{
	if (_instance == nullptr)
	{
		_instance = new Global();
	}
	return _instance;
}

void Global::Free()
{
	if (_instance)
	{
		delete(_instance);
		_instance = 0;
	}
}