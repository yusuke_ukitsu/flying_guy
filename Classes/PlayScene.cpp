//インクルード
#include "PlayScene.h"
#include "Global.h"
#include "Result.h"
#include "Title.h"
#include "Define.h"
#include "SimpleAudioEngine.h"

//日本語使用可
#pragma execution_character_set("utf-8")

//───────────────────────────
//コンストラクタ
//───────────────────────────
PlayScene::PlayScene() :
//背景スクロールのスタート地点
BG_MOVE_START(240, 1580),
//画面外の座標(弾に使用)
OVER_POSITION(10000, 70),
//スコアの基本加算値
ADD_POINT(40)
{
	//ゲーム進行に必要な値の初期化
	playInit();

	//使用するファイル関連の初期化
	fileInit();

	//弾関連の初期化
	bulletInit();

	//主人公の移動関連の初期化
	moveInit();

	//スコア関連の初期化
	scoreInit();
}

//───────────────────────────
//デストラクタ
//───────────────────────────
PlayScene::~PlayScene()
{


}

//───────────────────────────
//シーン作成
//引数：なし
//戻値：作成したシーンのアドレス
//───────────────────────────
Scene* PlayScene::createScene()
{
	//シーンの作成
	auto scene = Scene::create();

	//レイヤーの作成
	auto layer = PlayScene::create();

	//シーンにレイヤーを登録
	scene->addChild(layer);

	//シーンを返す
	return scene;
}

//───────────────────────────
//初期化（シーンの最初にやること）
//引数：なし
//戻値：初期化できたかどうか
//───────────────────────────
bool PlayScene::init()
{
	//シーンができてなかったら中断
	if (!Layer::init())
	{
		return false;
	}

	//背景を作成
	createBG();

	//弾を作成
	createbullet();

	//エネミーを作成
	createEnemy();
	
	//主人公を作成
	_main = createNode("main.png", Vec2(305, 120));

	//スコアボード・レーン作る
	createNode("score.png", Vec2(240, 320));

	//スコアを作成
	_scoreLabel = createLabel("count2.png", 28, 36, '0', Vec2(65, 530));

	//スコアを初期化
	changeLabel(_scoreLabel, 0);

	//ハイスコアを作成
	auto label = createLabel("count2.png", 28, 36, '0', Vec2(65, 430));
	
	//ハイスコアを初期化
	setHiScore(label);

	//ポーズ画面作成
	_pause = createNode("pause.png", Vec2(240, 320));

	//ポーズ画面を非表示
	_pause->setVisible(false);

	//ゲームオーバー画面作成
	_gameOver = createNode("Guy.png", Vec2(310, 340));

	//ゲームオーバー画面を非表示
	_gameOver->setVisible(false);
	
	//スタートアニメーションを再生
	startAnimation(Vec2(310, 340), _startCount, _startFile);

	//弾用の乱数生成器を準備する
	srand((unsigned)time(NULL));

	//BGMプリロード(あらかじめロードしてスタンバイ)
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic("music/PLAY.mp3");

	//BGMスタート
	CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("music/PLAY.mp3", true);

	//入力処理
	input();

	//定期的にupdate関数を呼ぶ
	this->scheduleUpdate();

	return true;
}

//───────────────────────────
//更新（常にやること）
//引数：dt　前回更新してからの時間（秒）
//戻値：なし
//───────────────────────────
void PlayScene::update(float dt)
{
	//スタートカウント
	{
		//スタートカウント開始
		_start -= dt;

		//カウントが0以下だったら
		if ((int)_start <= 0)
		{
			//ゲームをプレイ状態にする
			_gameState |= PLAYING;
		}
	}

	//プレイ中だったら
	if (_gameState == PLAYING)
	{
		//背景のスクロール(各々下に移動させる)
		BGMove();

		//弾の移動
		bulletMove();

		//スコア計算
		scoreCount(dt);

		//プレイヤーの移動処理
		playerMove();

		//当たり判定
		hitEvent();
	}

	//主人公が消滅したら
	if (_main == nullptr)
	{
		//ゲームオーバの表示
		gameOver(dt);
	}
}

//───────────────────────────
//入力処理
//引数：なし
//戻値：なし
//───────────────────────────
void PlayScene::input()
{
	//キーボードイベント
	auto keyListener = EventListenerKeyboard::create();

	//キーが押されたとき
	keyListener->onKeyPressed = [this](EventKeyboard::KeyCode keyCode, Event* event) {
		switch (keyCode)
		{
			//「→」が押されたとき
		case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
			//右に移動
			rightMove();
			break;

			//「←」が押されたとき
		case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
			//左に移動
			leftMove();
			break;

			//「ENTER」が押されたとき
		case EventKeyboard::KeyCode::KEY_KP_ENTER:
			//リザルト画面に移動
			resultMove();
			break;

			//「ESCAPE」が押されたとき
		case EventKeyboard::KeyCode::KEY_ESCAPE:
			//ポーズ画面の表示or非表示
			pauseEvent();
			break;

			//「T」が押されたとき
		case EventKeyboard::KeyCode::KEY_T:
			//タイトル画面に移動
			titleMove();
			break;
		}
	};

	//キーボード監視
	this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyListener, this);
}

//───────────────────────────
//ゲーム進行に必要な値の初期化
//引数：なし
//戻値：なし
//製作者:浮津　優輔
//───────────────────────────
void PlayScene::playInit()
{
	//難易度の取得
	_level = Global::getInstance()->Lvfg / 5;

	//ゲームの状態をリセット
	_gameState = 0;

	//リザルトフラグをfalseにする
	_result = false;

	//スピードを初期値にする
	_speed = 1;
}

//───────────────────────────
//主人公の移動関連の初期化
//引数：なし
//戻値：なし
//製作者:浮津　優輔
//───────────────────────────
void PlayScene::moveInit()
{
	//移動できるマスを計算して格納
	int playerCount = sizeof(_usPos) / sizeof(_usPos[0]);

	//マス分繰り返す
	for (int i = 0; i < playerCount; i++)
	{
		//移動する座標を配列に格納
		_usPos[i] = Vec2(CHARACTER_POSITION_X + CHARACTER_WIDTH * i, PLAYER_HEGHT);
	}

	//初期位置を真ん中にする
	_posi = CENTER;
}

//───────────────────────────
//スコア関連の初期化
//引数：なし
//戻値：なし
//製作者:浮津　優輔
//───────────────────────────
void PlayScene::scoreInit()
{
	//加算スコアを作成
	_addScore = ADD_POINT * _level;

	//時間を初期化
	_time = 0;

	//スコアを初期化
	_score = 0;
}

//───────────────────────────
//弾関連の初期化
//引数：なし
//戻値：なし
//製作者:浮津　優輔
//───────────────────────────
void PlayScene::bulletInit()
{
	//弾の数を計算して格納
	_bulletCount = sizeof(_bullet) / sizeof(_bullet[0]);

	//弾の数分繰り返す
	for (int i = 0; i < _bulletCount; i++)
	{
		//弾の初期位置を設定
		_bulletPos[i] = OVER_POSITION;
	}
}

//───────────────────────────
//使用するファイル関連の初期化
//引数：なし
//戻値：なし
//製作者:浮津　優輔
//───────────────────────────
void PlayScene::fileInit()
{
	//開始アニメ
	{
		//開始アニメに使用するコマを格納
		char* start[] = { "deadex.png", "cnt3.png", "cnt2.png", "cnt1.png", "start.png" };

		//内容をコピー
		memcpy(_startFile, start, sizeof(start));

		//コマ数を求めて、格納する
		_startCount = sizeof(start) / sizeof(start[0]);

		//アニメの再生時間を求める
		_start = 1.0f * _startCount;
	}
	
	//死亡アニメ
	{
		//死亡アニメに使用するコマを格納
		char* deth[] = { "deadex.png", "ef/dead0000.png", "ef/dead0001.png", "ef/dead0002.png", "ef/dead0003.png" };

		//内容をコピー
		memcpy(_dethFile, deth, sizeof(deth));

		//コマ数を求めて、格納する
		_dethCount = sizeof(deth) / sizeof(deth[0]);

		//アニメの再生時間を求める
		_deth = 0.4 * _dethCount + 1;
	}
	

	//敵
	{
		//敵の画像を格納
		char* enemy[] = { "enemy1.png", "enemy2.png", "enemy3.png" };

		//内容をコピー
		memcpy(_enemyFile, enemy, sizeof(enemy));

		//敵の画像数を求めて格納
		_enemyCount = sizeof(_enemy) / sizeof(_enemy[0]);
	}
	
	

	//背景
	{
		//背景の画像を格納
		char* scroll[] = { "scroll.png", "scroll1.png", "scroll2.png" };

		//内容をコピー
		memcpy(_scrollFile, scroll, sizeof(scroll));

		//背景の画像数を求めて格納
		_bgCount = sizeof(_scroll) / sizeof(_scroll[0]);
	}
	
}

//───────────────────────────
//背景の作成
//引数：なし
//戻値：なし
//製作者:浮津　優輔
//───────────────────────────
void PlayScene::createBG()
{
	//背景の数分繰り返す
	for (int i = 0; i < _bgCount; i++)
	{
		//背景を作る
		_scroll[i] = createNode(_scrollFile[i], Vec2(SCROLL_WIDTH, SCREEN_HEIGHT / 2 + SCREEN_HEIGHT * i));
	}
}

//───────────────────────────
//弾の作成
//引数：なし
//戻値：なし
//製作者:浮津　優輔
//───────────────────────────
void PlayScene::createbullet()
{
	//弾の数分繰り返す
	for (int i = 0; i < _bulletCount; i++)
	{
		//弾を作る
		_bullet[i] = createNode("bullet1.png", _bulletPos[i]);
	}
}

//───────────────────────────
//敵の作成
//引数：なし
//戻値：なし
//製作者:浮津　優輔
//───────────────────────────
void PlayScene::createEnemy()
{
	//敵の数分繰り返す
	for (int i = 0; i < _enemyCount; i++)
	{
		//敵を作る
		_enemy[i] = createNode(_enemyFile[i], Vec2(CHARACTER_POSITION_X + CHARACTER_WIDTH * i, ENEMY_HEIGHT));
	}
}

//───────────────────────────
//ノードの作成
//引数：ファイル名(char* str), 表示位置(Vec2 position)
//戻値：作成されたノード(Sprite* node)
//製作者:浮津　優輔
//───────────────────────────
Sprite* PlayScene::createNode(char* str, Vec2 position)
{
	//ノードを作成する
	auto node = Sprite::create(str);

	//位置を指定
	node->setPosition(position);

	//シーンに追加
	this->addChild(node);

	//作成したノードを返す
	return node;
}

//───────────────────────────
//ラベルの作成
//引数：ファイル名(char* str), 画像の幅(int width), 画像の高さ(int height)
//引数：初期数値(int startNumber), 表示位置(Vec2 position)
//戻値：なし
//製作者:浮津　優輔
//───────────────────────────
Label* PlayScene::createLabel(char* str, int width, int height, int startNumber, Vec2 position)
{
	//ラベルを作成
	auto label = Label::createWithCharMap(str, width, height, startNumber);

	//位置を設定
	label->setPosition(position);

	//シーンに追加
	this->addChild(label);

	//ラベルにシャドウをつける
	label->enableShadow();

	//作成したラベルを返す
	return label;
}

//───────────────────────────
//ラベルの変更
//引数：変更するラベル(Label* label)
//戻値：なし
//製作者:浮津　優輔
//───────────────────────────
void PlayScene::changeLabel(Label* label, int number)
{
	//変更用のラベルを作成
	auto str = String::createWithFormat("%d", number);

	//ラベルの内容を更新する
	label->setString(str->getCString());
}

//───────────────────────────
//ハイスコアの初期化
//引数：変更するラベル(Label* label)
//戻値：なし
//製作者:浮津　優輔
//───────────────────────────
void PlayScene::setHiScore(Label* label)
{
	//取り出し用の要素数作成
	int set = _level - 1;

	//ハイスコアが存在しない場合
	if ((Global::getInstance()->HiScore[set]) < 0)

		//0で初期化
		changeLabel(label, 0);
	//ハイスコアがある場合
	else

		//ロードして表示
		changeLabel(label, Global::getInstance()->HiScore[set]);
}

//───────────────────────────
//アニメーションの再生
//引数：表示位置(Vec2 position),アニメのコマ数(int count),アニメのファイル(char* file[])
//引数：最初の画像に戻すかどうか(bool set),再生速度(float speed)
//補足：デフォルト値->set(true),sreed(1.0f)
//戻値：なし
//製作者:浮津　優輔
//───────────────────────────
void PlayScene::startAnimation(Vec2 position, int count, char* file[], bool set, float speed)
{
	//アニメーション開始用ノードを作る
	auto start = createNode("deadex.png", position);

	//アニメーションを作成
	auto animation = createAnimation(count, file, set, speed);

	//アニメーションを再生する
	start->runAction(animation);
}

//───────────────────────────
//アニメーションの作成
//引数：アニメのコマ数(int count),アニメのファイル(char* file[])
//引数：最初の画像に戻すかどうか(bool set),再生速度(float speed)
//補足：デフォルト値set(true),sreed(1.0f)
//戻値：作成されたアニメ(Animate* animate)
//製作者:浮津　優輔
//───────────────────────────
Animate* PlayScene::createAnimation(int count, char* file[], bool set, float speed)
{
	//アニメーションを作る
	auto anime = Animation::create();

	//コマ数分繰り返す
	for (int i = 0; i < count; i++)
	{
		//アニメーションにコマを登録
		anime->addSpriteFrameWithFileName(file[i]);
	}

	//最初の画像に戻すかどうか
	anime->setRestoreOriginalFrame(set);

	//アニメーシ-ン速度
	anime->setDelayPerUnit(speed);

	//アニメーションを元にアニメを作成
	auto animate = Animate::create(anime);

	//作成したアニメを返す
	return animate;
}


//───────────────────────────
//背景のスクロール
//引数：なし
//戻値：なし
//製作者:浮津　優輔
//───────────────────────────
void PlayScene::BGMove()
{
	//移動値を作成
	Vec2 move(0, SCROLL_SPEED);

	//背景の画像分繰り返す
	for (int i = 0; i < _bgCount; i++)
	{
		//背景を下に移動させる
		_scroll[i]->setPosition(_scroll[i]->getPosition() - move);

		//画面外に出たら上のスクロール待ちに戻す
		if (_scroll[i]->getPosition().y <= SCREEN_OUT)
		{
		//背景スクロールの開始位置に移動
			_scroll[i]->setPosition(BG_MOVE_START);
		}
	}
}

//───────────────────────────
//弾の移動
//引数：なし
//戻値：なし
//製作者:浮津　優輔
//───────────────────────────
void PlayScene::bulletMove()
{
	//弾の数分繰り返す
	for (int i = 0; i < _bulletCount;i++)
	{
		//弾の移動値を計算
		_bulletPos[i].y -= (Global::getInstance()->Lvfg + _speed);

		//弾の現在地に移動値を設定する
		_bullet[i]->setPosition(_bulletPos[i]);
	}

	//画面外まで行った
	if (_bulletPos[0].y < 0)
	{
		//発射する弾を設定
		patternSelct();
	}
}

//───────────────────────────
//発射する弾のパターンを選択
//引数：なし
//戻値：なし
//製作者:浮津　優輔
//───────────────────────────
void PlayScene::patternSelct()
{
	//ランダムで弾のパターンを選択
	switch ((int)rand() % 3)
	{
	case 0:
		//弾の位置をUFOの位置(左端、中央)にする
		shotPosition(LEFT, CENTER);
		break;
	case 1:
		//弾の位置をUFOの位置(左端、右端)にする
		shotPosition(LEFT, RIGHT);
		break;
	case 2:
		//弾の位置をUFOの位置(中央、右端)にする
		shotPosition(CENTER, RIGHT);
		break;
	default:
		break;
	}
}

//───────────────────────────
//発射する弾の位置を指定する
//引数：なし
//戻値：なし
//製作者:浮津　優輔
//───────────────────────────
void PlayScene::shotPosition(int e1, int e2)
{
	//弾の発射位置を設定
	_bulletPos[0] = _enemy[e1]->getPosition();
	_bulletPos[1] = _enemy[e2]->getPosition();
}

//───────────────────────────
//スコア計算
//引数：時間(float dt)
//戻値：なし
//製作者:浮津　優輔
//───────────────────────────
void PlayScene::scoreCount(float dt)
{
	//タイムカウント開始
	_time += dt;

	//スコアの計算
	_score = (int)_time * _addScore;

	//スピードupの基本値を作成
	int set = ((_level) / 3) * 100;

	//スコアが設定値を超えたら
	if (_score / (set + 100) == (2 * _speed))

		//スピードを増やす
		_speed += 1;

	//スコアを更新
	changeLabel(_scoreLabel, _score);
}

//───────────────────────────
//プレイヤーの移動
//引数：なし
//戻値：なし
//製作者:浮津　優輔
//───────────────────────────
void PlayScene::playerMove()
{
	//右に移動
	//左に移動
	//移動先の座標を配列から取得する
	Vec2	nextPos = _usPos[_posi];

	//主人公を移動させる
	_main->setPosition(nextPos);
}

//───────────────────────────
//当たり判定
//引数：なし
//戻値：なし
//製作者:浮津　優輔
//───────────────────────────
void PlayScene::hitEvent()
{
	//主人公を囲む四角形
	Rect main = _main->getBoundingBox();

	//主人公と弾が接触した場合
	if ((main.containsPoint(_bullet[0]->getPosition())) || (main.containsPoint(_bullet[1]->getPosition())))
	{
		//当たったときの処理
		dethEvent();
	}
}

//───────────────────────────
//ゲームオーバーイベント
//引数：時間(float dt)
//戻値：なし
//製作者:浮津　優輔
//───────────────────────────
void PlayScene::gameOver(float dt)
{
	//ゲームの状態を「ゲームオーバー」に変更
	_gameState |= GAME_OVER;

	//死亡アニメの再生時間を減らす
	_deth -= dt;

	//アニメの再生が終わったら
	if ((int)_deth <= 0)
	{
		//ゲームオーバー画面作成
		_gameOver->setVisible(true);
		//createNode("Guy.png", Vec2(310, 340));

		//現在のスコアを保存する
		Global::getInstance()->Score = _score;

		//ENTERでリザルトへ移動可にするフラグ
		_result = true;
	}
}

//───────────────────────────
//死亡イベント
//引数：時間(float dt)
//戻値：なし
//製作者:浮津　優輔
//───────────────────────────
void PlayScene::dethEvent()
{
	//BGMを停止する
	CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic("music/PLAY.mp3");

	//アニメーションを再生
	startAnimation(_main->getPosition(), _dethCount, _dethFile, true, 0.2f);

	//爆破音再生
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("music/GameOver.mp3");

	//主人公を親から削除
	_main->removeFromParent();

	//主人公のポインタをヌルポインターにしておく
	_main = nullptr;
}

//───────────────────────────
//左に移動
//引数：なし
//戻値：なし
//製作者:浮津　優輔
//───────────────────────────
void PlayScene::leftMove()
{
	//左端より大きい時
	if (_posi>LEFT)
	{
		//左に移動
		_posi = _posi - CENTER;
	}
}

//───────────────────────────
//右に移動
//引数：なし
//戻値：なし
//製作者:浮津　優輔
//───────────────────────────
void PlayScene::rightMove()
{
	//右端より小さい時
	if (_posi<RIGHT)
	{
		//右に移動
		_posi = _posi + CENTER;
	}
}

//───────────────────────────
//ポーズ画面の表示or非表示
//引数：なし
//戻値：なし
//製作者:浮津　優輔
//───────────────────────────
void PlayScene::pauseEvent()
{
	//ゲームオーバーでなかったら
	if (!(_gameState & GAME_OVER))
	{
		//ポーズ中だったら
		if (_gameState & PAUSE)
		{
			//ポーズ画面を非表示
			_pause->setVisible(false);

			//ゲームの状態を「プレイ中」に変更
			_gameState &= PLAYING;
		}
		//プレイ中だったら
		else if (_gameState & PLAYING)
		{
			//ポーズ画面を表示
			_pause->setVisible(true);

			//ゲームの状態を「ポーズ中」に変更
			_gameState |= PAUSE;
		}
	}
	
}

//───────────────────────────
//タイトル画面に移動
//引数：なし
//戻値：なし
//製作者:浮津　優輔
//───────────────────────────
void PlayScene::titleMove()
{
	//ポーズ中だったら
	if (_gameState & PAUSE)
	{
		//BGMの再生を止める
		CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic("music/BGM2ex.mp3");

		//タイトルシーンに移動
		auto transition = TransitionFade::create(1.0f, Title::createScene(), Color3B::WHITE);

		//暗転しながら画面を変える
		Director::getInstance()->replaceScene(transition);
	}
}

//───────────────────────────
//リザルト画面に移動
//引数：なし
//戻値：なし
//製作者:浮津　優輔
//───────────────────────────
void PlayScene::resultMove()
{
	//リザルトがtrueだったら
	if (_result == true)
	{
		//リザルトに切り替え
		auto transition = TransitionMoveInT::create(1.0f, Result::createScene());

		//暗転してシーン移動
		Director::getInstance()->replaceScene(transition);
	}
}