#pragma once

//インクルード
#include "cocos2d.h"

//namespace
USING_NS_CC;

//××シーンクラス
class Result :public Layer
{
protected:
	Result();                       //コンストラクタ
	virtual ~Result();              //デストラクタ
	bool init() override;           //初期化（シーンの最初にやること）
	void update(float dt) override; //更新（常にやること）
	void input();                   //入力処理


	//メンバを追加する場合は以下に書く

	int _sfg;//シーン切り替えフラグ
	int _cnt;
	Label*	_label[2];
	const char*	_KEY;




public:
	static Scene* createScene();    //シーン作成
	CREATE_FUNC(Result);            //ｺﾝﾋﾞﾆｴﾝｽｺﾝｽﾄﾗｸﾀ
};