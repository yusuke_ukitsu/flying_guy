//マクロ定義
#define SCREEN_OUT -320				//画面外の座標
#define SCREEN_HEIGHT 640			//画面の高さ

#define SCROLL_SPEED 5				//スクロールの速さ
#define SCROLL_WIDTH 240			//スクロールの配置位置(横座標)

#define CHARACTER_WIDTH 71			//キャラクター間の横幅
#define CHARACTER_POSITION_X 233	//キャラクターの配置位置(横座標)
#define PLAYER_HEGHT 120			//プレイヤーの配置位置(縦座標)
#define ENEMY_HEIGHT 600

//ゲームの進行状態を表す
#define	PLAYING 0x04				//プレイ中
#define	PAUSE 0x02					//ポーズ中
#define	GAME_OVER 0x01				//ゲームオーバー