//インクルード
#include "Result.h"
#include "Title.h"
#include "Global.h"
#include "SimpleAudioEngine.h"

//日本語使用可
#pragma execution_character_set("utf-8")

//───────────────────────────
//コンストラクタ
//───────────────────────────
Result::Result()
{
	//フラグ初期化
	_sfg = 0;
	_cnt = 0;

}

//───────────────────────────
//デストラクタ
//───────────────────────────
Result::~Result()
{



}

//───────────────────────────
//シーン作成
//引数：なし
//戻値：作成したシーンのアドレス
//───────────────────────────
Scene* Result::createScene()
{
	auto scene = Scene::create();
	auto layer = Result::create();
	scene->addChild(layer);
	return scene;
}

//───────────────────────────
//初期化（シーンの最初にやること）
//引数：なし
//戻値：初期化できたかどうか
//───────────────────────────
bool Result::init()
{
	//シーンができてなかったら中断
	if (!Layer::init())
	{
		return false;
	}
	//キーの登録
	const char*  HIGHSCORE_KEY = "highscoreKey";
	const char*  HIGHSCORE_KEY2 = "highscoreKey2";
	const char*  HIGHSCORE_KEY3 = "highscoreKey3";
	const char*  HIGHSCORE_KEY4 = "highscoreKey4";
	Global::getInstance()->_KEY = HIGHSCORE_KEY;
	Global::getInstance()->_KEY2 = HIGHSCORE_KEY2;
	Global::getInstance()->_KEY3 = HIGHSCORE_KEY3;
	Global::getInstance()->_KEY4 = HIGHSCORE_KEY4;
	//セーブ機能の実装
	auto userDefault = UserDefault::getInstance();



	auto BackG = Sprite::create("result2.png");		//背景を作る
	BackG->setPosition(240, 320);               //位置を指定
	this->addChild(BackG);                    //シーンに追加

	{
		auto label = Label::createWithCharMap("cnt11.png", 40, 48, '0');//フォント画像を読み込み設定
		this->addChild(label);
		label->setPosition(290, 382);
		_label[0] = label;
	}
	
	auto str = String::createWithFormat("%d点", Global::getInstance()->Score);//プレイスコアをセット
		_label[0]->setString(str->getCString());
	
	{
		auto label = Label::createWithCharMap("cnt11.png", 40, 48, '0');//ハイスコアのセット
		this->addChild(label);
		label->setPosition(290, 335);
		_label[1] = label;
	}

	//ハイスコアのセーブ
	
	
	//セーブデータのロード
	{
		//レベル1のスコア
		if ((Global::getInstance()->Lvfg / 5) == 1)
		{
			int highscore = userDefault->getIntegerForKey(HIGHSCORE_KEY);
			if (Global::getInstance()->Score > highscore)
			{
				_cnt = 1;
				//データのセーブ(名前、データ)
				UserDefault::getInstance()->setIntegerForKey(HIGHSCORE_KEY, Global::getInstance()->Score);
				auto str2 = String::createWithFormat("%d点", Global::getInstance()->Score);
				_label[1]->setString(str2->getCString());
			}
			else
			{
				auto str2 = String::createWithFormat("%d点", highscore);
				_label[1]->setString(str2->getCString());
			}
		}
		//レベル2のスコア
		else if ((Global::getInstance()->Lvfg / 5) == 2)
		{
			int highscore = userDefault->getIntegerForKey(HIGHSCORE_KEY2);
			if (Global::getInstance()->Score > highscore)
			{
				_cnt = 1;
				//データのセーブ(名前、データ)
				UserDefault::getInstance()->setIntegerForKey(HIGHSCORE_KEY2, Global::getInstance()->Score);
				auto str2 = String::createWithFormat("%d点", Global::getInstance()->Score);
				_label[1]->setString(str2->getCString());
			}
			else
			{
				auto str2 = String::createWithFormat("%d点", highscore);
				_label[1]->setString(str2->getCString());
			}
		}
		//レベル3のスコア
		else if ((Global::getInstance()->Lvfg / 5) == 3)
		{
			int highscore = userDefault->getIntegerForKey(HIGHSCORE_KEY3);
			if (Global::getInstance()->Score > highscore)
			{
				_cnt = 1;
				//データのセーブ(名前、データ)
				UserDefault::getInstance()->setIntegerForKey(HIGHSCORE_KEY3, Global::getInstance()->Score);
				auto str2 = String::createWithFormat("%d点", Global::getInstance()->Score);
				_label[1]->setString(str2->getCString());
			}
			else
			{
				auto str2 = String::createWithFormat("%d点", highscore);
				_label[1]->setString(str2->getCString());
			}
		}
		//レベル4のスコア
		else if ((Global::getInstance()->Lvfg / 5) == 4)
		{
			int highscore = userDefault->getIntegerForKey(HIGHSCORE_KEY4);
			if (Global::getInstance()->Score > highscore)
			{
				_cnt = 1;
				//データのセーブ(名前、データ)
				UserDefault::getInstance()->setIntegerForKey(HIGHSCORE_KEY4, Global::getInstance()->Score);
				auto str2 = String::createWithFormat("%d点", Global::getInstance()->Score);
				_label[1]->setString(str2->getCString());
			}
			else
			{
				auto str2 = String::createWithFormat("%d点", highscore);
				_label[1]->setString(str2->getCString());
			}
		}
	}
	
	

	

	if (_cnt==1)//ハイスコア更新時
	{
		auto hyouka = Label::createWithTTF("さすがだな！\nしかしわたしの記録を越すことは\nできなかったな！", "fonts/HGRPP1.TTC", 23);
		hyouka->setPosition(230, 200);
		hyouka->enableShadow();
		this->addChild(hyouka);
	}
	//ハイスコア更新でない場合(スコア2500以上)
	else if ((Global::getInstance()->Score >= 2500) && (_cnt == 0))
	{
		auto hyouka = Label::createWithTTF("貴様もそろそろ慣れてきたようだな!!", "fonts/HGRPP1.TTC", 23);
		hyouka->setPosition(235, 200);
		hyouka->enableShadow();
		this->addChild(hyouka);
	}

	//ハイスコア更新でない場合(スコア2000以上)
	else if ((Global::getInstance()->Score >= 2000) && (_cnt == 0))
	{
		auto hyouka = Label::createWithTTF("ぶぁかめ!!\n今のは貴様なら避けれただろう!!", "fonts/HGRPP1.TTC", 23);
		hyouka->setPosition(230, 200);
		hyouka->enableShadow();
		this->addChild(hyouka);
	}

	//ハイスコア更新でない場合(スコア1000以上)
	else if ((Global::getInstance()->Score >= 1000) && (_cnt == 0))
	{
		auto hyouka = Label::createWithTTF("なかなかやるな!!しかしまだ甘い!!", "fonts/HGRPP1.TTC", 23);
		hyouka->setPosition(230, 200);
		hyouka->enableShadow();
		this->addChild(hyouka);
	}

	//ハイスコア更新でない場合(スコア500以上)
	else if ((Global::getInstance()->Score >= 500) && (_cnt == 0))
	{
		auto hyouka = Label::createWithTTF("まだまだだな！その程度か！", "fonts/HGRPP1.TTC", 23);
		hyouka->setPosition(230, 200);
		hyouka->enableShadow();
		this->addChild(hyouka);
	}

	//ハイスコア更新でない場合(スコア500以下)
	else if ((Global::getInstance()->Score < 500) && (_cnt == 0))
	{
		auto hyouka = Label::createWithTTF("出直してこい！", "fonts/HGRPP1.TTC", 23);
		hyouka->setPosition(230, 200);
		hyouka->enableShadow();
		this->addChild(hyouka);
	}

	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("music/Result.mp3");

	//入力処理
	input();

	//定期的にupdate関数を呼ぶ
	this->scheduleUpdate();

	return true;
}

//───────────────────────────
//更新（常にやること）
//引数：dt　前回更新してからの時間（秒）
//戻値：なし
//───────────────────────────
void Result::update(float dt)
{	


	if (_sfg == 1)//エンターが押されたらタイトルに戻る
	{
		_sfg = 0;
		auto transition = TransitionFade::create(1.0f, Title::createScene(), Color3B::WHITE);
		Director::getInstance()->replaceScene(transition);
	}
}

//───────────────────────────
//入力処理
//引数：なし
//戻値：なし
//───────────────────────────
void Result::input()
{
	//キーボードイベント
	auto keyListener = EventListenerKeyboard::create();

	//キーが押されたとき
	keyListener->onKeyPressed = [this](EventKeyboard::KeyCode keyCode, Event* event) {
		switch (keyCode)
		{
			//「ENTER」が押されたとき
		case EventKeyboard::KeyCode::KEY_KP_ENTER://タイトルに移行

			_sfg = 1;

			break;
		};

	};

	//キーを放したとき
	keyListener->onKeyReleased = [this](EventKeyboard::KeyCode keyCode, Event* event) {

		switch (keyCode)
		{
			//「ENTER」を放したとき
		case EventKeyboard::KeyCode::KEY_ENTER:

			break;
					
		};

	};

	//キーボード監視
	this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyListener, this);




}