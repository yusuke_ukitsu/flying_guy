//インクルード
#include "Demo.h"
#include "Title.h"

//日本語使用可
#pragma execution_character_set("utf-8")

//───────────────────────────
//コンストラクタ
//───────────────────────────
Demo::Demo()
{



}

//───────────────────────────
//デストラクタ
//───────────────────────────
Demo::~Demo()
{



}

//───────────────────────────
//シーン作成
//引数：なし
//戻値：作成したシーンのアドレス
//───────────────────────────
Scene* Demo::createScene()
{
	auto scene = Scene::create();
	auto layer = Demo::create();
	scene->addChild(layer);
	return scene;
}

//───────────────────────────
//初期化（シーンの最初にやること）
//引数：なし
//戻値：初期化できたかどうか
//───────────────────────────
bool Demo::init()
{
	//シーンができてなかったら中断
	if (!Layer::init())
	{
		return false;
	}

	auto demoBG = Sprite::create("tutorial.png"); //スプライトを作る
	demoBG->setPosition(240, 320);                //位置を指定
	this->addChild(demoBG);                    //シーンに追加

	//入力処理
	input();

	//定期的にupdate関数を呼ぶ
	this->scheduleUpdate();

	return true;
}

//───────────────────────────
//更新（常にやること）
//引数：dt　前回更新してからの時間（秒）
//戻値：なし
//───────────────────────────
void Demo::update(float dt)
{

}

//───────────────────────────
//入力処理
//引数：なし
//戻値：なし
//───────────────────────────
void Demo::input()
{
	//キーボードイベント
	auto keyListener = EventListenerKeyboard::create();

	//キーが押されたとき
	keyListener->onKeyPressed = [this](EventKeyboard::KeyCode keyCode, Event* event) {
		switch (keyCode)
		{
			//「ENTER」が押されたとき
		case EventKeyboard::KeyCode::KEY_KP_ENTER://タイトルに移行
			auto transition = TransitionFade::create(1.0f, Title::createScene(), Color3B::WHITE);
			Director::getInstance()->replaceScene(transition);
			break;
		};
	};

	//キーボード監視
	this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyListener, this);




}