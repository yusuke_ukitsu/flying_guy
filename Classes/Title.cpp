//インクルード
#include "Title.h"
#include "Demo.h"
#include "PlayScene.h"
#include "Global.h"
#include "SimpleAudioEngine.h"

//日本語使用可
#pragma execution_character_set("utf-8")

//───────────────────────────
//コンストラクタ
//───────────────────────────
Title::Title()
{
	_sfg = 0;//フラグ初期化
	_lockfg = 0;//ロックフラグ初期化

	Global::getInstance()->Lvfg = 5;//Lvの初期化(5=ノーマル相当)
}

//───────────────────────────
//デストラクタ
//───────────────────────────
Title::~Title()
{



}

//───────────────────────────
//シーン作成
//引数：なし
//戻値：作成したシーンのアドレス
//───────────────────────────
Scene* Title::createScene()
{
	auto scene = Scene::create();
	auto layer = Title::create();
	scene->addChild(layer);
	return scene;
}

//───────────────────────────
//初期化（シーンの最初にやること）
//引数：なし
//戻値：初期化できたかどうか
//───────────────────────────
bool Title::init()
{
	//シーンができてなかったら中断
	if (!Layer::init())
	{
		return false;
	}
	//キーの登録
	const char*  HIGHSCORE_KEY = "highscoreKey";
	const char*  HIGHSCORE_KEY2 = "highscoreKey2";
	const char*  HIGHSCORE_KEY3 = "highscoreKey3";
	const char*  HIGHSCORE_KEY4 = "highscoreKey4";
	Global::getInstance()->_KEY = HIGHSCORE_KEY;
	Global::getInstance()->_KEY2 = HIGHSCORE_KEY2;
	Global::getInstance()->_KEY3 = HIGHSCORE_KEY3;
	Global::getInstance()->_KEY4 = HIGHSCORE_KEY4;
	//セーブ・ロードの実装
	auto userDefault = UserDefault::getInstance();


	//タイトル画像
	{
		auto title = Sprite::create("title.png"); //タイトルスプライトを作る
		title->setPosition(240, 320);                //位置を指定
		this->addChild(title);                    //シーンに追加
	}
	//レベルの表示
	{
		auto label = Label::createWithCharMap("cnt11.png", 40, 48, '0');//数字フォントの読み込み設定
		this->addChild(label);
		label->setPosition(320, 150);
		label->enableShadow();
		_level = label;
	}


	//ハイスコアの準備(管理用)
	//レベル1のデータをロード
	{
		int highscore = userDefault->getIntegerForKey(HIGHSCORE_KEY);
		Global::getInstance()->HiScore[0] = highscore;
	}
	
	//レベル2のデータをロード
	{
		int highscore = userDefault->getIntegerForKey(HIGHSCORE_KEY2);
		Global::getInstance()->HiScore[1] = highscore;
	}
	//レベル3のデータをロード
	{
		int highscore = userDefault->getIntegerForKey(HIGHSCORE_KEY3);
		Global::getInstance()->HiScore[2] = highscore;
	}
	//レベル4のデータをロード
	{
		int highscore = userDefault->getIntegerForKey(HIGHSCORE_KEY4);
		Global::getInstance()->HiScore[3] = highscore;
	}


	//タイトルBGMのプリロード(あらかじめ読み込んでスタンバイ)
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic("music/tilte.mp3");

	//タイトルBGMの再生
	CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("music/title.mp3", true);


	//入力処理
	input();

	//定期的にupdate関数を呼ぶ
	this->scheduleUpdate();

	return true;
}

//───────────────────────────
//更新（常にやること）
//引数：dt　前回更新してからの時間（秒）
//戻値：なし
//───────────────────────────
void Title::update(float dt)
{
	if (_sfg == 1)//シーンフラグ1,エンターキーが押された場合の処理
	{
		_sfg = 0;
		auto transition = TransitionFadeBL::create(1.0f, PlayScene::createScene());//ゲーム画面に切り替え
		Director::getInstance()->replaceScene(transition);
		
	}

	else if (_sfg == 2)//シーンフラグ2,Tキーが押された場合の処理
	{
		_sfg = 0;
		auto transition = TransitionFadeBL::create(1.0f, Demo::createScene());//チュートリアルに切り替え
		Director::getInstance()->replaceScene(transition);
	}
	else if (_sfg == 4 && _lockfg != 1 && ((Global::getInstance()->Lvfg) < 20))//↑キーが押されたらレベルを一つ上げる(Lv4まで)
	{
		Global::getInstance()->Lvfg += 5;
		_lockfg = 1;
	}

	else if (_sfg == 5 && _lockfg != 1 && ((Global::getInstance()->Lvfg) > 5))//下キーを押されたらLvを一つ下げる(Lv1まで)
	{
		Global::getInstance()->Lvfg -= 5;
		_lockfg = 1;
	}

	//レベルラベルlevelの作成
	auto level = String::createWithFormat("%d", Global::getInstance()->Lvfg/5);

	//レベルをラベルに送信
	_level->setString(level->getCString());

}

//───────────────────────────
//入力処理
//引数：なし
//戻値：なし
//───────────────────────────
void Title::input()
{
	//キーボードイベント
	auto keyListener = EventListenerKeyboard::create();

	//キーが押されたとき
	keyListener->onKeyPressed = [this](EventKeyboard::KeyCode keyCode, Event* event) {
		switch (keyCode)
		{
			//「ENTER」が押されたとき
		case EventKeyboard::KeyCode::KEY_KP_ENTER:

			_sfg = 1;
			CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic("music/title.mp3");
			break;

			//「T」が押されたとき
		case EventKeyboard::KeyCode::KEY_T:

			//auto transition = TransitionFadeBL::create(1.0f, Demo::createScene());//チュートリアルに切り替え
			//Director::getInstance()->replaceScene(transition);
			_sfg = 2;
			
			break;

			//「ESC」が押されたとき
		case EventKeyboard::KeyCode::KEY_ESCAPE:

			if ((Global::getInstance()->Lvfg / 5) == 1)//難易度1/N(推奨)
			{
				UserDefault::getInstance()->setIntegerForKey(Global::getInstance()->_KEY, 0);
				Global::getInstance()->HiScore[0] = 0;
			}
			else if ((Global::getInstance()->Lvfg / 5) == 2)//難易度2/H
			{
				UserDefault::getInstance()->setIntegerForKey(Global::getInstance()->_KEY2, 0);
				Global::getInstance()->HiScore[1] = 0;
			}
			else if ((Global::getInstance()->Lvfg / 5) == 3)//難易度3/VH
			{
				UserDefault::getInstance()->setIntegerForKey(Global::getInstance()->_KEY3, 0);
				Global::getInstance()->HiScore[2] = 0;
			}
			else if ((Global::getInstance()->Lvfg / 5) == 4)//難易度4/EX
			{
				UserDefault::getInstance()->setIntegerForKey(Global::getInstance()->_KEY4, 0);
				Global::getInstance()->HiScore[3] = 0;
			}
			break;

			//「↑」が押されたとき
		case EventKeyboard::KeyCode::KEY_UP_ARROW:

			_sfg = 4;

			break;

			//「↓」が押されたとき
		case EventKeyboard::KeyCode::KEY_DOWN_ARROW:

			_sfg = 5;

			break;
		};
	};

	//キーを放したとき
	keyListener->onKeyReleased = [this](EventKeyboard::KeyCode keyCode, Event* event) {

		switch (keyCode)
		{
			//「ENTER」を放したとき
		case EventKeyboard::KeyCode::KEY_ENTER:

			break;

			//「T」を放したとき
		case EventKeyboard::KeyCode::KEY_T:

			break;

			//「ESC」を離したとき
		case EventKeyboard::KeyCode::KEY_ESCAPE:

			break;

			//「↑」を離したとき
		case EventKeyboard::KeyCode::KEY_UP_ARROW:

			_sfg = 0;//シーンフラグをリセット
			_lockfg = 0;//繰り返し防止フラグをリセット

			break;

			//「↓」を離したとき
		case EventKeyboard::KeyCode::KEY_DOWN_ARROW:

			_sfg = 0;//シーンフラグをリセット
			_lockfg = 0;//繰り返し防止フラグリセット

			break;
		};

	};

	//キーボード監視
	this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyListener, this);




}