//============================================
//省略名
//・BG -> BackGround(背景)
//============================================
#pragma once

//インクルード
#include "cocos2d.h"

//namespace
USING_NS_CC;

//××シーンクラス
class PlayScene :public Layer
{
//============================================
//コンストラクタ処理で使用する関数及び関連する関数
//============================================

	//ゲーム進行に必要な値の初期化
	void playInit();

	//使用するファイル関連の初期化
	void fileInit();

	//弾関連の初期化
	void bulletInit();

	//主人公の移動関連の初期化
	void moveInit();

	//スコア関連の初期化
	void scoreInit();

//============================================
//init処理で使用する関数及び関連する関数
//============================================

	//背景の作成
	void createBG();

	//弾の作成
	void createbullet();

	//敵の作成
	void createEnemy();

	//ノードの作成
	Sprite* createNode(char* str, Vec2 position);

	//ラベルの作成
	Label*	createLabel(char* str, int width, int height, int startNumber, Vec2 position);

	//ラベルの変更
	void changeLabel(Label* label, int number);

	//ハイスコアの初期化
	void setHiScore(Label* label);

	//アニメーションの再生
	void startAnimation(Vec2 position, int count, char* file[], bool set = true, float speed = 1.0f);

	//アニメーションの作成
	Animate* createAnimation(int count, char* file[], bool set = true, float speed = 1.0f);

//============================================
//update処理で使用する関数及び関連する関数
//============================================
	//背景の移動
	void BGMove();

	//弾の移動
	void bulletMove();

	//発射する弾のパターンを選択
	void patternSelct();

	//発射する弾の位置を指定する
	void shotPosition(int e1, int e2);

	//スコア計算
	void scoreCount(float dt);

	//プレイヤーの移動
	void playerMove();

	//当たり判定
	void hitEvent();

	//ゲームオーバーイベント
	void gameOver(float dt);

	//死亡イベント
	void dethEvent();

//============================================
//input処理で使用する関数及び関連する関数
//============================================

	//左に移動
	void leftMove();

	//右に移動
	void rightMove();

	//ポーズ画面の表示or非表示
	void pauseEvent();

	//タイトル画面に移動
	void titleMove();

	//リザルト画面に移動
	void resultMove();

//
protected:
	//コンストラクタ
	PlayScene();

	//デストラクタ
	virtual ~PlayScene();

	//初期化（シーンの最初にやること）
	bool init() override;

	//更新（常にやること）
	void update(float dt) override;

	//入力処理
	void input();

//============================================
//メンバ変数を追加する場合は以下に書く
//============================================
	//定数
	const Vec2	BG_MOVE_START;		//背景スクロールのスタート地点
	const Vec2	OVER_POSITION;		//画面外の座標
	const int	ADD_POINT;			//基本加算値

	//変数
	Sprite*		_main;				//主人公の変数
	Sprite*		_enemy[3];			//敵の配列型変数
	Sprite*		_bullet[2];			//弾の配列型変数
	Sprite*		_pause;				//ポーズの変数
	Sprite*		_gameOver;			//ゲームオーバーの変数
	Sprite*		_scroll[3];			//スクロールの配列型変数
	Label*		_scoreLabel;		//残り時間の表示

	Vec2		_bulletPos[2];		//弾の移動値を格納する配列型変数
	Vec2		 _usPos[3];			//主人公移動座標を格納する配列型変数

	char*		_scrollFile[256];	//背景に使用する画像を格納する配列型変数
	char*		_enemyFile[256];	//敵に使用する画像を格納する配列型変数
	char*		_startFile[256];	//開始アニメに使用する画像を格納する配列型変数
	char*		_dethFile[256];		//死亡アニメに使用する画像を格納する配列型変数

	float		_time;				//時間の変数
	float		_start;				//開始アニメの再生時間
	float		_deth;				//死亡アニメの再生時間

	bool		_result;			//リザルトのフラグ

	int			_score;				//スコア
	int			_addScore;			//加算スコア
	int			_level;				//難易度
	int			_speed;				//スピード
	int			_posi;				//移動の添え字
	int			_bgCount;			//背景画像の数
	int			_bulletCount;		//弾画像の数
	int			_enemyCount;		//敵画像の数
	int			_startCount;		//開始アニメのコマ数
	int			_dethCount;			//死亡アニメのコマ数

	short		_gameState;			//ゲームの進行状態を表す変数

	//位置(主人公,敵)
	enum
	{
		LEFT,	//左
		CENTER,	//中央
		RIGHT	//右
	};

public:
	//シーン作成
	static Scene* createScene();

	//PlayScene作成
	CREATE_FUNC(PlayScene);
};
