#pragma once

//インクルード
#include "cocos2d.h"

//namespace
USING_NS_CC;

//××シーンクラス
class Title :public Layer
{
protected:
	Title();                       //コンストラクタ
	virtual ~Title();              //デストラクタ
	bool init() override;           //初期化（シーンの最初にやること）
	void update(float dt) override; //更新（常にやること）
	void input();                   //入力処理


	//メンバを追加する場合は以下に書く

	int _sfg;//シーン切り替えフラグ
	int _lockfg;//繰り返し防止フラグ

	Label* _level;



public:
	static Scene* createScene();    //シーン作成
	CREATE_FUNC(Title);            //ｺﾝﾋﾞﾆｴﾝｽｺﾝｽﾄﾗｸﾀ

};